package com.module.admin.prj.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.module.admin.prj.pojo.PrjAnt;

/**
 * prj_ant的Dao
 * @author autoCode
 * @date 2018-01-30 20:32:37
 * @version V1.0.0
 */
public interface PrjAntDao {

	public abstract void save(PrjAnt prjAnt);

	public abstract void update(PrjAnt prjAnt);

	public abstract void delete(@Param("paId")Integer paId);

	public abstract PrjAnt get(@Param("paId")Integer paId);

	public abstract List<PrjAnt> findPrjAnt(PrjAnt prjAnt);
	
	public abstract int findPrjAntCount(PrjAnt prjAnt);
}