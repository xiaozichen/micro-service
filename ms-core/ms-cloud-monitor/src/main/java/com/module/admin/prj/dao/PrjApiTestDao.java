package com.module.admin.prj.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.module.admin.prj.pojo.PrjApiTest;

/**
 * prj_api_test的Dao
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
public interface PrjApiTestDao {

	public abstract void save(PrjApiTest prjApiTest);

	public abstract void update(PrjApiTest prjApiTest);

	public abstract void delete(@Param("id")Integer id);

	public abstract PrjApiTest get(@Param("id")Integer id);

	public abstract List<PrjApiTest> findPrjApiTest(PrjApiTest prjApiTest);
	
	public abstract int findPrjApiTestCount(PrjApiTest prjApiTest);

	public abstract void updateTestResult(@Param("id")Integer id, @Param("testResult")String testResult);
}