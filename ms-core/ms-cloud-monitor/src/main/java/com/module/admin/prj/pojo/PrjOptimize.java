package com.module.admin.prj.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * prj_optimize实体
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Alias("prjOptimize")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PrjOptimize extends BaseEntity implements Serializable {
	//编码
	private Integer id;
	//项目编号
	private Integer prjId;
	//接口地址
	private String url;
	//预警时间[单位ms]
	private Integer warnTime;
	//创建时间
	private Date createTime;
	
	//============================ 扩展属性
	//项目名称
	private String prjName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getPrjId() {
		return prjId;
	}
	public void setPrjId(Integer prjId) {
		this.prjId = prjId;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Integer getWarnTime() {
		return warnTime;
	}
	public void setWarnTime(Integer warnTime) {
		this.warnTime = warnTime;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getPrjName() {
		return prjName;
	}
	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}
}