<%@page import="com.module.admin.prj.enums.PrjInfoContainer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑API测试</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="patId" value="${param.patId}">
		<input type="hidden" id="path" value="${prjApiTestDtl.path}">
		<input type="hidden" id="params" value="${prjApiTestDtl.params}">
		<div class="form-group">
			<label for="path" class="col-sm-4">路径 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="pathInfo" placeholder="路径" value="${prjApiTestDtl.path}" data-provide="typeahead" autocomplete="off" ></div>
		</div>
		<div class="form-group" id="paramsPanel">
			<label for="path" class="col-sm-4"></label>
			<div class="col-sm-8">参数信息（根据路径加载参数信息）</div>
		</div>
		<div class="form-group">
			<label for="succCond" class="col-sm-4">成功条件 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="succCond" placeholder="成功条件" value="${prjApiTestDtl.succCond}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
			apis : [${apiString}],/* ${prjApiString}, */
			apiList : {},
			initParams : function(item) {
				$('#path').val(info.apiList[item].path);
            	var pObj = eval('('+info.apiList[item].params+')');
            	var params = ['<table class="table table-striped table-hover">',
            					'<thead><tr class="info">',
                            	'<th width="130">参数</th>',
                                '<th width="180">值</th>',
                                '<th>描叙</th>',
                                '<th width="150">类型</th>',
                                '</tr>',
                            '</thead>',
                            '<tbody>'];
            	$.each(pObj, function(i, obj) {
            		params.push('<tr><td>',obj.code,info.required=='true'?' <span class="text-danger">*</span>':'','</td>',
                      		'<td><input type="text" class="params" id="params_',obj.code,'" name="',obj.code,'" style="width: 150px;" value="',obj.value,'"/></td>',
                      		'<td>',obj.name,'</td>',
                      		'<td>',obj.clazz,'</td></tr>');
            	});
            	params.push('</tbody></table>');
            	$('#paramsPanel').empty().append(params.join(''));
			}
	};
	$(function() {
		//var dataSource = [];
		$.each(info.apis, function(i, obj) {
			var value = obj.path + (obj.name?' ('+ obj.name +')':'');
			//dataSource.push(value);
			info.apiList[obj.path] = obj;
			info.apiList[value] = obj;
		});
		
        $('#pathInfo').typeahead({
        	//数据源
            //source: dataSource,
            source: function (query, process) {
                //query是输入的值
                return $.ajax({url:webroot + '/prjApi/f-json/findSearch.shtml?prjId=${prjApiTest.prjId}',
                		data: { searchString: query },
                		type: 'post',
                		dataType: 'json',
                		success: function (json) {
                    if (json.code === 0) {
                        //if (e.body.length == 0) { alert("没有查到对应的人"); return; }
                        var array = [];
                        $.each(json.body, function (i, obj) {
                            //name2Id[ele.path] = ele.path;//键值对保存下来。
                            //array.push(obj.path);
                            
                            var value = obj.path + (obj.name?' ('+ obj.name +')':'');
                			//dataSource.push(value);
                			info.apiList[obj.path] = obj;
                			info.apiList[value] = obj;
                			
                            array.push(value);
                        });
                        process(array);
                    }
                }});
            },
            afterSelect: function (item) {
            	//选择项之后的事件，item是当前选中的选项
            	info.initParams(item);
            }
        });
        

		if('${prjApiTestDtl.params}'==='') {
			$('#succCond').val('code=0');
		} else {
			//编辑
			info.initParams('${prjApiTestDtl.path}');
			//设置编辑时的值
			<c:if test="${prjApiTestDtl.params!=null}">
			var params = ${prjApiTestDtl.params};
			$.each(params, function(i, obj) {
				$('#params_' + i).val(obj);
			});
			</c:if>
		}
		
        
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			_saveMsg.attr('class', 'label label-danger');
			var patId = $('#patId').val();
			var path = $('#path');
			if(JUtil.isEmpty(path.val())) {
				_saveMsg.append('请输入路径');
				path.focus();
				return;
			}
			var params = [];
			$('.params').each(function(i, obj) {
				var name = $(obj).attr('name');
				var value = $(obj).val();
				params.push('"'+name+'":"'+value+'"');
			});
			if(params.length == 0) {
				_saveMsg.append('请输入参数内容');
				return;
			}

			var succCond = $('#succCond');
			if(JUtil.isEmpty(succCond.val())) {
				_saveMsg.append('请输入成功条件');
				succCond.focus();
				return;
			}
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/prjApiTestDtl/f-json/save.shtml',
				data : {
					patId: patId,
					path: path.val(),
					params: '{'+params.join(',')+'}',
					succCond: succCond.val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.apiDtl.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>