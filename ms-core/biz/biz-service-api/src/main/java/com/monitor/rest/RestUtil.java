package com.monitor.rest;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.monitor.MonitorCons;
import com.ms.env.EnvUtil;
import com.system.auth.AuthUtil;
import com.system.comm.utils.FrameHttpUtil;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

@Component
public class RestUtil {

	@Autowired
	private LoadBalancerClient loadBalancer;
	@Autowired
	private RestTemplate restTemplate;

	public String getRestUrl(String serviceId, String fallbackUri) {
		URI uri = null;
		try {
			ServiceInstance instance = loadBalancer.choose(serviceId);
			uri = instance.getUri();
		} catch (RuntimeException e) {
			uri = URI.create(fallbackUri);
		}
		return uri.toString();
	}

	/**
	 * 发送请求[如果是开发环境，需要配置dev.host.服务的serviceId=http://xxx.xxx.xx.xx:xx]
	 * @param serviceId
	 * @param url
	 * @param params
	 * @param fallbackUri
	 * @return
	 */
	public ResponseFrame request(String serviceId, String url, Map<String, Object> params, String fallbackUri) {
		//判断是否为开发环境
		String baseUrl = null;
		if(EnvUtil.projectModeIsDev()) {
			baseUrl = EnvUtil.get("dev.host." + serviceId);
		}
		if (baseUrl == null) {
			baseUrl = getRestUrl(serviceId, fallbackUri);
		}
		long begin = System.currentTimeMillis();
		String resResult = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			//  请勿轻易改变此提交方式，大部分的情况下，提交方式都是表单提交
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			//  封装参数，千万不要替换为Map与HashMap，否则参数无法传递
			MultiValueMap<String, String> paramsMvm = new LinkedMultiValueMap<String, String>();
			Iterator<Entry<String, Object>> entryKeyIterator = params.entrySet().iterator();
			while (entryKeyIterator.hasNext()) {
				Entry<String, Object> e = entryKeyIterator.next();
				String value = null;
				if(e.getValue() != null) {
					value = e.getValue().toString();
				}
				paramsMvm.add(e.getKey(), value);
			}
			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(paramsMvm, headers);
			resResult = restTemplate.postForObject(baseUrl + url, requestEntity, String.class);
			//String result = FrameHttpUtil.post(baseUrl + url, params);
			ResponseFrame frame = FrameJsonUtil.toObject(resResult, ResponseFrame.class);
			/*if(ResponseCode.SUCC.getCode() != frame.getCode()) {
				String remark = "接口响应异常!";
				saveOptimizeLog(serviceId, url, params, begin, null, null, resResult, remark);
			}*/
			return frame;
		} catch (Exception e) {
			ResponseFrame frame = new ResponseFrame(ResponseCode.SERVER_ERROR);
			Map<String, Object> body = new HashMap<String, Object>();
			body.put("requestParams", params);
			body.put("exceptionInfo", e.getMessage());
			frame.setBody(body);
			return frame;
		} finally {
			long end = System.currentTimeMillis();
			long time = end - begin;
			if(time > 2000) {
				//接口请求>2s，则发送优化信息
				String remark = "请求超时!";
				saveOptimizeLog(serviceId, url, params, begin, end, time, resResult, remark);
			}
		}
	}
	
	private void saveOptimizeLog(final String serviceId, final String url, final Map<String, Object> params,
			long begin, Long end, Long time, final String resResult, final String remark) {
		if(end == null) {
			end = System.currentTimeMillis();
		}
		if(time == null) {
			time = end - begin;
		}
		final String reqTime = FrameTimeUtil.parseString(new Date(begin));
		final String resTime = FrameTimeUtil.parseString(new Date(end));
		final long useTime = time;
		ThreadPoolTaskExecutor pool = FrameSpringBeanUtil.getBean(ThreadPoolTaskExecutor.class);
		pool.execute(new Runnable() {
			@Override
			public void run() {
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				try {
					paramsMap.put("code", serviceId);
					paramsMap.put("url", url);
					paramsMap.put("params", FrameJsonUtil.toString(params));
					paramsMap.put("reqTime", reqTime);
					paramsMap.put("resTime", resTime);
					paramsMap.put("useTime", useTime);
					paramsMap.put("resResult", resResult);
					paramsMap.put("remark", remark);
					post("/api/prjOptimizeLog/save", paramsMap);
				} catch (IOException e) {
				}
			}
		});
	}

	/**
	 * 请求服务端的api
	 * @param url
	 * @param paramsMap
	 * @return
	 * @throws IOException 
	 */
	private ResponseFrame post(String url, Map<String, Object> paramsMap) throws IOException {
		String time = String.valueOf(System.currentTimeMillis());
		paramsMap.put("clientId", MonitorCons.clientId);
		paramsMap.put("time", time);
		paramsMap.put("sign", AuthUtil.auth(MonitorCons.clientId, time, MonitorCons.sercret));
		String result = FrameHttpUtil.post(MonitorCons.serverHost + url, paramsMap);
		return FrameJsonUtil.toObject(result, ResponseFrame.class);
	}
}